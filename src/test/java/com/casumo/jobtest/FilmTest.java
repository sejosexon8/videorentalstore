package com.casumo.jobtest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.casumo.jobtest.dto.FilmDto;
import com.casumo.jobtest.entity.Film;
import com.casumo.jobtest.repository.FilmRepository;
import com.casumo.jobtest.service.FilmService;
import com.casumo.jobtest.type.FilmCategory;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * Test class related to film entity
 * 
 * @author Dean Meden
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class FilmTest {
	
	private static final Logger log = LoggerFactory.getLogger(FilmTest.class);
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	FilmRepository filmRepository;
	
	@Test
	public void testSaveFilm() {
		
		log.info("testing saving film");
		
		FilmDto filmDto = new FilmDto();
		filmDto.setCategory(FilmCategory.NEW);
		filmDto.setName("Matrix");
		
		Long filmId = filmService.saveNewFilm(filmDto);
		
        Film film = filmRepository.findById(filmId);
        
        assertThat(film.getFilmCategory(), is(FilmCategory.NEW));
        assertThat(film.getName(), is("Matrix"));		
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void testSaveCustomerDataMissing() {
		
		log.info("testing saving film with data missing");
		
		FilmDto filmDto = new FilmDto();
		filmDto.setCategory(FilmCategory.NEW);
		
		filmService.saveNewFilm(filmDto);		
	}

}
