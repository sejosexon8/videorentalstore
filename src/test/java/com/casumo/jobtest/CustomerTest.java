package com.casumo.jobtest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.casumo.jobtest.dto.CustomerDto;
import com.casumo.jobtest.dto.TopCustomerDto;
import com.casumo.jobtest.entity.Customer;
import com.casumo.jobtest.repository.CustomerRepository;
import com.casumo.jobtest.service.CustomerService;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test class related to customer entity
 * 
 * @author Dean Meden
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class CustomerTest {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerTest.class);
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	CustomerRepository customerRepository;
		
	@Test
	public void testSaveCustomer() {
		
		log.info("testing saving customer");
		
		CustomerDto customerDto = new CustomerDto();
		customerDto.setAddress("Ilica");
		customerDto.setFirstName("Dean");
		customerDto.setLastName("Meden");
		
		Long customerId = customerService.saveNewCustomer(customerDto);
		
        Customer customer = customerRepository.findById(customerId);
        
        assertThat(customer.getAddress(), is("Ilica"));
        assertThat(customer.getFirstName(), is("Dean"));
        assertThat(customer.getLastName(), is("Meden"));
		
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void testSaveCustomerDataMissing() {
		
		log.info("testing saving customer with data missing");
		
		CustomerDto customerDto = new CustomerDto();
		customerDto.setAddress("Ilica");
		customerDto.setFirstName("Dean");
		
		customerService.saveNewCustomer(customerDto);
		
	}
	
	@Test
	@DatabaseSetup("/testData/customerTestData.xml")
	public void testGetTopCustomers() {
		
		log.info("testing getting top customers");
		
		List<TopCustomerDto> customers= customerService.getTopCustomers();
		
		//check number of top customers, it should be 3 of them
		assertThat(customers.size(), is(3));
		
		//check order of top customers
		assertThat(customers.get(0).getCustomerId(), is(4L));
		assertThat(customers.get(1).getCustomerId(), is(2L));
		assertThat(customers.get(2).getCustomerId(), is(1L));		
	}

}
