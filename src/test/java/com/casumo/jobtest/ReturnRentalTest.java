package com.casumo.jobtest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.casumo.jobtest.dto.ReturnFilmsDto;
import com.casumo.jobtest.entity.Rental;
import com.casumo.jobtest.exception.VideoRentalException;
import com.casumo.jobtest.repository.CustomerRepository;
import com.casumo.jobtest.repository.RentalRepository;
import com.casumo.jobtest.service.RentalService;
import com.casumo.jobtest.type.RentalStatus;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test class related to return films process
 * 
 * @author Dean Meden
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"/testData/customerTestData.xml", "/testData/filmTestData.xml"})
public class ReturnRentalTest {
	
	private static final Logger log = LoggerFactory.getLogger(ReturnRentalTest.class);
	
	@Autowired
	RentalService rentalService;
	
	@Autowired
	RentalRepository rentalRepository; 
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
			
	@Test
	public void testReturnRentalNoFilms() {
				
		log.info("testing return rent with no films");
						
		List<ReturnFilmsDto> returnFilmsDtoList = new ArrayList<ReturnFilmsDto>();
		
		expectedEx.expect(VideoRentalException.class);
	    expectedEx.expectMessage("No films to return error");
		
		rentalService.returnRentedFilms(returnFilmsDtoList);		
	}
	
	@Test
	@DatabaseSetup("/testData/rentalTestData.xml")
	public void testReturnRentalNoAdditionalPrice() {
		
		log.info("testing return rent before deadline");
		
		List<ReturnFilmsDto> returnFilmsDtoList = new ArrayList<ReturnFilmsDto>();
		
		ReturnFilmsDto returnFilmsDto1 = new ReturnFilmsDto(); 
		returnFilmsDto1.setRentalId(1L);
		returnFilmsDtoList.add(returnFilmsDto1);
		
		ReturnFilmsDto returnFilmsDto2 = new ReturnFilmsDto(); 
		returnFilmsDto2.setRentalId(2L);
		returnFilmsDtoList.add(returnFilmsDto2);
		
		Integer additionalPrice = rentalService.returnRentedFilms(returnFilmsDtoList);	
		
		assertThat(additionalPrice, is(0));
		
		Rental rental1 = rentalRepository.findById(1L);
		Rental rental2 = rentalRepository.findById(2L);
		
		assertThat(rental1.getRentalStatus(), is(RentalStatus.FINISHED));
		assertThat(rental2.getRentalStatus(), is(RentalStatus.FINISHED));
		
		assertThat(rental1.getFinalPrice(), is(rental1.getInitialPrice()));
		assertThat(rental2.getFinalPrice(), is(rental2.getInitialPrice()));
				
	}
	
	@Test
	@DatabaseSetup("/testData/rentalTestData.xml")
	public void testReturnRentalWithStatusFinished() {
		
		log.info("testing return rent before deadline");
		
		List<ReturnFilmsDto> returnFilmsDtoList = new ArrayList<ReturnFilmsDto>();
		
		ReturnFilmsDto returnFilmsDto1 = new ReturnFilmsDto(); 
		returnFilmsDto1.setRentalId(1L);
		returnFilmsDtoList.add(returnFilmsDto1);
		
		Rental rental1 = rentalRepository.findById(1L);
		rental1.setRentalStatus(RentalStatus.FINISHED);
		rentalRepository.saveAndFlush(rental1);
		
		expectedEx.expect(VideoRentalException.class);
	    expectedEx.expectMessage("Trying to return rental that is already in state FINISHED");
	    				
		rentalService.returnRentedFilms(returnFilmsDtoList);			
	}
	
	//test case from specification
	@Test
	@DatabaseSetup("/testData/rentalTestData.xml")
	public void testReturnRentalWithWithAdditionalPrice() {
		
		log.info("testing return rent after deadline");
		
		List<ReturnFilmsDto> returnFilmsDtoList = new ArrayList<ReturnFilmsDto>();
		
		ReturnFilmsDto returnFilmsDto1 = new ReturnFilmsDto(); 
		returnFilmsDto1.setRentalId(1L);
		returnFilmsDtoList.add(returnFilmsDto1);
		
		ReturnFilmsDto returnFilmsDto2 = new ReturnFilmsDto(); 
		returnFilmsDto2.setRentalId(2L);
		returnFilmsDtoList.add(returnFilmsDto2);
		
		Rental rental1 = rentalRepository.findById(1L);
		rental1.setStartTime(ZonedDateTime.now(ZoneId.systemDefault()).minusDays(2));
		rentalRepository.saveAndFlush(rental1);
		
		Rental rental2 = rentalRepository.findById(2L);
		rental2.setStartTime(ZonedDateTime.now(ZoneId.systemDefault()).minusDays(5));
		rentalRepository.saveAndFlush(rental2);
				    				
		Integer additionalPrice = rentalService.returnRentedFilms(returnFilmsDtoList);	
		
		assertThat(additionalPrice, is(110));	
		
		rental1 = rentalRepository.findById(1L);
		rental2 = rentalRepository.findById(2L);
		
		assertThat(rental1.getRentalStatus(), is(RentalStatus.FINISHED));
		assertThat(rental2.getRentalStatus(), is(RentalStatus.FINISHED));
		
		assertThat(rental1.getFinalPrice(), is(120));
		assertThat(rental2.getFinalPrice(), is(120));
	}
	
	@After
	public void clean() {
		log.info("clean rentals");
		rentalRepository.deleteAll();
	}
	
	

}
