package com.casumo.jobtest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test class which runs all tests together
 * 
 * @author Dean Meden
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ CustomerTest.class, FilmTest.class, RentalTest.class, ReturnRentalTest.class} )
public final class AllTestsSuite {}
