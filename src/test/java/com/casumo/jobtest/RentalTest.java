package com.casumo.jobtest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.casumo.jobtest.dto.RentFilmsDto;
import com.casumo.jobtest.dto.SingleRentFilmDto;
import com.casumo.jobtest.entity.Customer;
import com.casumo.jobtest.exception.VideoRentalException;
import com.casumo.jobtest.repository.CustomerRepository;
import com.casumo.jobtest.repository.RentalRepository;
import com.casumo.jobtest.service.RentalService;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test class related to rental process
 * 
 * @author Dean Meden
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"/testData/customerTestData.xml", "/testData/filmTestData.xml"})
public class RentalTest {
	
	private static final Logger log = LoggerFactory.getLogger(RentalTest.class);
	
	@Autowired
	RentalService rentalService;
	
	@Autowired
	RentalRepository rentalRepository; 
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void testRentalCustomerNotExist() {
		
		log.info("testing rental with non existing customer");
		
		RentFilmsDto rentFilmsDto = new RentFilmsDto();
		rentFilmsDto.setCustomerId(5L);	
		
	    expectedEx.expect(VideoRentalException.class);
	    expectedEx.expectMessage("Customer does not exist");
		
	    rentalService.saveRentedFilms(rentFilmsDto);	  
	}
	
	@Test
	public void testRentalNoFilms() {
		
		log.info("testing rental with no films");
		
		RentFilmsDto rentFilmsDto = new RentFilmsDto();
		rentFilmsDto.setCustomerId(1L);	
		
		expectedEx.expect(VideoRentalException.class);
	    expectedEx.expectMessage("No films to rent error");
		
		rentalService.saveRentedFilms(rentFilmsDto);		
	}
	
	@Test
	public void testRentalOneFilm() {
		
		log.info("testing rental with one film");
		
		RentFilmsDto rentFilmsDto = new RentFilmsDto();
		rentFilmsDto.setCustomerId(1L);
		
		List<SingleRentFilmDto> films = new ArrayList<SingleRentFilmDto>();	
		
		SingleRentFilmDto film = new SingleRentFilmDto();
		film.setFilmId(1L);
		film.setRentalPeriodDays(5);
		films.add(film);
		
		rentFilmsDto.setFilms(films);
		
		Integer price = rentalService.saveRentedFilms(rentFilmsDto);
		Customer customer = customerRepository.findById(1L);
		
		assertThat(price, is(200));
		
		//check if bonus points are increased by 2, starting value is 33 for customer with id=1
		assertThat(customer.getPoints(), is(35));	
		
	}
	
	//test case from specification
	@Test
	public void testRentalMultipleFilms() {
		
		log.info("testing rental with multiple films");
		
		RentFilmsDto rentFilmsDto = new RentFilmsDto();
		rentFilmsDto.setCustomerId(2L);
		
		List<SingleRentFilmDto> films = new ArrayList<SingleRentFilmDto>();	
		
		SingleRentFilmDto film1 = new SingleRentFilmDto();
		film1.setFilmId(1L);
		film1.setRentalPeriodDays(1);
		films.add(film1);
		
		SingleRentFilmDto film2 = new SingleRentFilmDto();
		film2.setFilmId(2L);
		film2.setRentalPeriodDays(5);
		films.add(film2);
		
		SingleRentFilmDto film3 = new SingleRentFilmDto();
		film3.setFilmId(3L);
		film3.setRentalPeriodDays(2);
		films.add(film3);
		
		SingleRentFilmDto film4 = new SingleRentFilmDto();
		film4.setFilmId(4L);
		film4.setRentalPeriodDays(7);
		films.add(film4);
		
		rentFilmsDto.setFilms(films);
		
		Integer price = rentalService.saveRentedFilms(rentFilmsDto);
		Customer customer = customerRepository.findById(2L);
		
		assertThat(price, is(250));
		
		//check if bonus points are increased by 5, starting value is 53 for customer with id=2
		assertThat(customer.getPoints(), is(58));	
	}
	
	@After
	public void clean() {
		log.info("clean rentals");
		rentalRepository.deleteAll();
	}
	
}
