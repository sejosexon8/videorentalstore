package com.casumo.jobtest.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppProperties {
	
	@Value("${videorental.price.premium}")
    private Integer premiumPrice;   
    @Value("${videorental.price.basic}")
    private Integer basicPrice; 
    
    @Value("${videorental.points.new}")
    private Integer pointsNew;   
    @Value("${videorental.points.other}")
    private Integer pointsOther;
    
	public Integer getPremiumPrice() {
		return premiumPrice;
	}
	public void setPremiumPrice(Integer premiumPrice) {
		this.premiumPrice = premiumPrice;
	}
	public Integer getBasicPrice() {
		return basicPrice;
	}
	public void setBasicPrice(Integer basicPrice) {
		this.basicPrice = basicPrice;
	}
	public Integer getPointsNew() {
		return pointsNew;
	}
	public void setPointsNew(Integer pointsNew) {
		this.pointsNew = pointsNew;
	}
	public Integer getPointsOther() {
		return pointsOther;
	}
	public void setPointsOther(Integer pointsOther) {
		this.pointsOther = pointsOther;
	} 
    
    

}
