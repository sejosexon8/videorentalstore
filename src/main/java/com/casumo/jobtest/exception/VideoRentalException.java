package com.casumo.jobtest.exception;

/**
 * Class defines custom exception used in for application error states
 * 
 * @author Dean Meden
 */

public class VideoRentalException extends RuntimeException {
	
	private static final long serialVersionUID = 3789252423033362059L;
	
	public VideoRentalException() {}

	public VideoRentalException(String message) {
        super(message);
    }
	
}
