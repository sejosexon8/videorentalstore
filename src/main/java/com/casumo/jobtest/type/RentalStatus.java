package com.casumo.jobtest.type;

/**
 * Enum defines rental statuses
 * 
 * @author Dean Meden
 */

public enum RentalStatus {
	
	IN_PROGRESS,
	FINISHED

}
