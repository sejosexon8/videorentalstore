package com.casumo.jobtest.type;

/**
 * Enum defines film categories
 * 
 * @author Dean Meden
 */

public enum FilmCategory {
	
	NEW,
	REGULAR,
	OLD;
	
}
