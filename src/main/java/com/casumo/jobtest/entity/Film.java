package com.casumo.jobtest.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.casumo.jobtest.type.FilmCategory;
import com.casumo.jobtest.util.ZonedDateTimeConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;;

/**
 * Class defines Film entity data
 * 
 * @author Dean Meden
 */

@Entity
@Table(name = "film")
public class Film extends BaseEntity {
	
	@Column(nullable = false)
	private String name;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
    private FilmCategory filmCategory;
	
	@JsonSerialize(converter = ZonedDateTimeConverter.Serializer.class)
	@JsonDeserialize(converter = ZonedDateTimeConverter.Deserializer.class)
	@Column(nullable = false, name="creation_time")
	private ZonedDateTime creationTime;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FilmCategory getFilmCategory() {
		return filmCategory;
	}

	public void setFilmCategory(FilmCategory filmCategory) {
		this.filmCategory = filmCategory;
	}	
	
	public ZonedDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(ZonedDateTime creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "Film [" + (name != null ? "name=" + name + ", " : "")
				+ (filmCategory != null ? "filmCategory=" + filmCategory + ", " : "")
				+ (creationTime != null ? "creationTime=" + creationTime : "") + "]";
	}
		
}
