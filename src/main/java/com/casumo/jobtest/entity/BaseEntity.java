package com.casumo.jobtest.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Superclass which is extended by other entity classes 
 * 
 * @author Dean Meden
 */

@MappedSuperclass
public class BaseEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "BaseEntity [" + (id != null ? "id=" + id : "") + "]";
	}
		
}
