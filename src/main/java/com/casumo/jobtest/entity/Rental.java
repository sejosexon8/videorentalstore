package com.casumo.jobtest.entity;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.casumo.jobtest.type.RentalStatus;
import com.casumo.jobtest.util.ZonedDateTimeConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class defines Rental entity data
 * 
 * @author Dean Meden
 */

@Entity
@Table(name = "rental")
public class Rental extends BaseEntity {
					
	@ManyToOne
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
	private Customer customer;
	
	@OneToOne
	@JoinColumn(name="film_id")
	private Film film;
	
	@JsonSerialize(converter = ZonedDateTimeConverter.Serializer.class)
	@JsonDeserialize(converter = ZonedDateTimeConverter.Deserializer.class)
	@Column(nullable = false, name="start_time")
	private ZonedDateTime startTime;
	
	@JsonSerialize(converter = ZonedDateTimeConverter.Serializer.class)
	@JsonDeserialize(converter = ZonedDateTimeConverter.Deserializer.class)
	@Column(nullable = true, name="end_time")
	private ZonedDateTime endTime;
	
	@Column(nullable = false, name="rent_days")
	private Integer rentDays;
	
	@Column(nullable = false, name="initial_price")
	private Integer initialPrice;
	
	@Column(nullable = true, name="final_price")
	private Integer finalPrice;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
    private RentalStatus rentalStatus;
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
			
	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}	

	public Integer getRentDays() {
		return rentDays;
	}

	public void setRentDays(Integer rentDays) {
		this.rentDays = rentDays;
	}

	public Integer getInitialPrice() {
		return initialPrice;
	}

	public void setInitialPrice(Integer initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Integer getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Integer finalPrice) {
		this.finalPrice = finalPrice;
	}
		
	public RentalStatus getRentalStatus() {
		return rentalStatus;
	}

	public void setRentalStatus(RentalStatus rentalStatus) {
		this.rentalStatus = rentalStatus;
	}

	@Override
	public String toString() {
		return "Rental [" + (customer != null ? "customerId=" + customer.getId() + ", " : "")
				+ (film != null ? "filmId=" + film.getId() + ", " : "")
				+ (startTime != null ? "startTime=" + DateTimeFormatter.ofPattern("dd/MM/yyyy - hh:mm").format(startTime) + ", " : "")
				+ (endTime != null ? "endTime=" + DateTimeFormatter.ofPattern("dd/MM/yyyy - hh:mm").format(endTime) + ", " : "")
				+ (rentDays != null ? "rentDays=" + rentDays + ", " : "")
				+ (initialPrice != null ? "initialPrice=" + initialPrice + ", " : "")
				+ (finalPrice != null ? "finalPrice=" + finalPrice + ", " : "")
				+ (rentalStatus != null ? "rentalStatus=" + rentalStatus : "") + "]";
	}
		
}
