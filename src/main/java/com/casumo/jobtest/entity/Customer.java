package com.casumo.jobtest.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.casumo.jobtest.util.ZonedDateTimeConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Class defines Customer entity data
 * 
 * @author Dean Meden
 */

@Entity
@Table(name = "customer")
public class Customer extends BaseEntity {
	
	@Column(nullable = false, name="first_name")
	private String firstName;
	
	@Column(nullable = false, name="last_name")
	private String lastName;
	
	@Column(nullable = false)
	private String address;
	
	@JsonSerialize(converter = ZonedDateTimeConverter.Serializer.class)
	@JsonDeserialize(converter = ZonedDateTimeConverter.Deserializer.class)
	@Column(nullable = false, name="creation_time")
	private ZonedDateTime creationTime;
	
	@Column(nullable = false)
	private Integer points;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
    private List<Rental> rentals;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ZonedDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(ZonedDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	public void setRentals(List<Rental> rentals) {
		this.rentals = rentals;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Customer [" + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (creationTime != null ? "creationTime=" + creationTime + ", " : "")
				+ (points != null ? "points=" + points + ", " : "")
				+ "]";
	}
	
}
