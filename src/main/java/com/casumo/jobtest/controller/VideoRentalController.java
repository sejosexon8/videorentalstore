package com.casumo.jobtest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.casumo.jobtest.dto.CustomerDto;
import com.casumo.jobtest.dto.FilmDto;
import com.casumo.jobtest.dto.RentFilmsDto;
import com.casumo.jobtest.dto.ReturnFilmsDto;
import com.casumo.jobtest.dto.TopCustomerDto;
import com.casumo.jobtest.service.CustomerService;
import com.casumo.jobtest.service.FilmService;
import com.casumo.jobtest.service.RentalService;

import io.swagger.annotations.ApiOperation;

/**
 * Rest controller for Video Rental Store  
 * 
 * @author Dean Meden
 */

@RestController
@RequestMapping("/videorental")
public class VideoRentalController {
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	RentalService rentalService;
	
	/**
	 * HTTP PUT method - Creates a new customer in database
	 * 
	 * @param  customerDto DTO with data needed to create new customer
	 * @return Id of new customer
	 */		
	@ApiOperation(value = "Create a new customer", response = Long.class)
	@RequestMapping(value = "/customer/new", 
    		method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Long createNewCustomer(@RequestBody CustomerDto customerDto) {		
		return customerService.saveNewCustomer(customerDto);		
    }
	
	/**
	 * HTTP PUT method - Creates a new film in database
	 * 
	 * @param  filmDto DTO with data needed to create new film
	 * @return Id of new film
	 */
	@ApiOperation(value = "Create a new film", response = Long.class)
	@RequestMapping(value = "/film/new", 
    		method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Long createNewFilm(@RequestBody FilmDto filmDto) {		
		return filmService.saveNewFilm(filmDto);		
    }
	
	/**
	 * HTTP POST method - Renting one or several films and calculating the price
	 * 
	 * @param  rentFilmsDto DTO with data related to films which are rented
	 * @return Total rent price 
	 */
	@ApiOperation(value = "Renting one or several films and calculating the price", response = Integer.class)
	@RequestMapping(value = "/rent", 
    		method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Integer rentFilms(@RequestBody RentFilmsDto rentFilmsDto) {		
		return rentalService.saveRentedFilms(rentFilmsDto);		
    }
	
	/**
	 * HTTP POST method - Returning films and calculating possible subcharges
	 * 
	 * @param  returnFilmsDtoList DTO with data related to films which are return to rental store
	 * @return Additional rent price if exist, otherwise 0  
	 */
	@ApiOperation(value = "Returning films and calculating possible subcharges", response = Integer.class)
	@RequestMapping(value = "/return", 
    		method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Integer returnFilms(@RequestBody List<ReturnFilmsDto> returnFilmsDtoList) {		
		return rentalService.returnRentedFilms(returnFilmsDtoList);		
    }
	
	/**
	 * HTTP GET method - Get top 3 customer with most bonus points
	 * 
	 * @return List of top 3 customers
	 */
	@ApiOperation(value = "Get top customers", response = List.class)
	@RequestMapping(value = "/customer/top", 
    		method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<TopCustomerDto> getTopCustomers() {		
		return customerService.getTopCustomers();		
    }

}
