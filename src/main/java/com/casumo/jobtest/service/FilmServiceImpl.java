package com.casumo.jobtest.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casumo.jobtest.dto.FilmDto;
import com.casumo.jobtest.entity.Film;
import com.casumo.jobtest.repository.FilmRepository;

/**
 * Class for all actions related to Video store Film 
 * 
 * @author Dean Meden
 */

@Service
public class FilmServiceImpl implements FilmService {
	
	private static final Logger log = LoggerFactory.getLogger(FilmServiceImpl.class);

	@Autowired
	FilmRepository filmRepository;
	
	/**
	 * Creates a new film in database
	 * 
	 * @param  filmDto DTO with data needed to create new film
	 * @return Id of new film
	 */
	@Override
	public Long saveNewFilm(FilmDto filmDto) {
		
		log.info("saveNewFilm: " + filmDto);
		
		Film film = new Film();
		film.setName(filmDto.getName());
		film.setFilmCategory(filmDto.getCategory());
		film.setCreationTime(ZonedDateTime.now(ZoneId.systemDefault()));
		
		film = filmRepository.saveAndFlush(film);
		
		return film.getId();
	}

}
