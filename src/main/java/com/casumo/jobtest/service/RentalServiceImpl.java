package com.casumo.jobtest.service;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casumo.jobtest.config.AppProperties;
import com.casumo.jobtest.dto.RentFilmsDto;
import com.casumo.jobtest.dto.ReturnFilmsDto;
import com.casumo.jobtest.dto.SingleRentFilmDto;
import com.casumo.jobtest.entity.Customer;
import com.casumo.jobtest.entity.Film;
import com.casumo.jobtest.entity.Rental;
import com.casumo.jobtest.exception.VideoRentalException;
import com.casumo.jobtest.repository.CustomerRepository;
import com.casumo.jobtest.repository.FilmRepository;
import com.casumo.jobtest.repository.RentalRepository;
import com.casumo.jobtest.type.FilmCategory;
import com.casumo.jobtest.type.RentalStatus;

/**
 * Class for all actions related to Video store Rental 
 * 
 * @author Dean Meden
 */
@Service
public class RentalServiceImpl implements RentalService {
	
	private static final Logger log = LoggerFactory.getLogger(RentalServiceImpl.class);
	
	@Autowired
	FilmRepository filmRepository;
	
	@Autowired
	RentalRepository rentalRepository; 
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	private AppProperties config;
	
	/**
	 * Stores multiple rental films. Rentals are saved inside rental entity in state IN_PROGRESS.
	 * Bonus points are also calculated and added to customer's points. Total rental price is returned. 
	 * 
	 * @param  rentFilmsDto DTO with data related to films which are rented
	 * @return Total rent price 
	 */	
	@Override
	@Transactional
	public Integer saveRentedFilms(RentFilmsDto rentFilmsDto)  {		
		
		log.info("saveRentedFilms start: " + rentFilmsDto);
		
		Customer customer = customerRepository.findById(rentFilmsDto.getCustomerId());
		
		if (customer == null) {	
			log.error("Customer does not exist");
			throw new VideoRentalException("Customer does not exist");
		}
		
		if (rentFilmsDto.getFilms() == null) {	
			log.error("No films to rent error");
			throw new VideoRentalException("No films to rent error");
		}
		
		Integer bonusPoints = customer.getPoints();
		
		Integer totalPrice = 0;	
						
		for (SingleRentFilmDto singleFilm : rentFilmsDto.getFilms()) {
			Film film = filmRepository.findById(singleFilm.getFilmId());
			Integer rentDays = singleFilm.getRentalPeriodDays();
			
			Rental rental = new Rental();
			rental.setCustomer(customer);
			rental.setStartTime(ZonedDateTime.now(ZoneId.systemDefault())); 
			rental.setRentDays(singleFilm.getRentalPeriodDays());
			rental.setFilm(film);
			
			Integer singlePrice = calculatePrice(film.getFilmCategory(), rentDays);						
			totalPrice += singlePrice;	
			
			bonusPoints = calculateBonusPoints(film.getFilmCategory(), bonusPoints);
			
			rental.setInitialPrice(singlePrice);
			rental.setRentalStatus(RentalStatus.IN_PROGRESS);
			rental = rentalRepository.saveAndFlush(rental);
			
			log.info("Rental saved: " + rental);
		}
				
		customer.setPoints(bonusPoints);
		
		log.info("Total bonus points: " + bonusPoints);
		log.info("Total price: " + totalPrice);
		
		customer = customerRepository.saveAndFlush(customer);
				
		log.info("saveRentedFilms finish");
		
		return totalPrice;
	}
	
	/**
	 * Update existing rental in state FINISHED and calculates additional rent price if initial
	 * rent days are exceeded.
	 * 
	 * @param  returnFilmsDtoList DTO with data related to films which are return to rental store
	 * @return Additional rent price if exist, otherwise 0  
	 */
	@Override
	@Transactional
	public Integer returnRentedFilms(List<ReturnFilmsDto> returnFilmsDtoList) {
		
		log.info("returnRentedFilms start: " + returnFilmsDtoList);
		
		if (returnFilmsDtoList == null || returnFilmsDtoList.size() == 0) {	
			log.error("No films to return error");
			throw new VideoRentalException("No films to return error");
		}
		
		Integer additionalRentPrice = 0;
		
		for (ReturnFilmsDto returnFilmsDto : returnFilmsDtoList)  {
						
			Rental rental = rentalRepository.findById(returnFilmsDto.getRentalId());
			
			if(rental.getRentalStatus() == RentalStatus.FINISHED) {
				throw new VideoRentalException("Trying to return rental that is already in state FINISHED");
			}
			
			ZonedDateTime startTime = rental.getStartTime();
			ZonedDateTime endTime = ZonedDateTime.now(ZoneId.systemDefault());
			
			Duration d = Duration.between(startTime, endTime);
			
			Long rentDiff = d.getSeconds();		
			Long realRentdays = TimeUnit.SECONDS.toDays(rentDiff)+1;
			
			Integer priceDiff = 0;
			if(realRentdays.intValue() > rental.getRentDays()) {
				Integer realRentPrice = calculatePrice(rental.getFilm().getFilmCategory(), realRentdays.intValue());	
				priceDiff = realRentPrice - rental.getInitialPrice();
				rental.setFinalPrice(realRentPrice);
				additionalRentPrice += priceDiff;
			}
			else {
				rental.setFinalPrice(rental.getInitialPrice());
			}
									
			rental.setRentalStatus(RentalStatus.FINISHED);
			rental.setEndTime(endTime);
			rental = rentalRepository.saveAndFlush(rental);
			
			log.info("Rental finished: " + rental);
						
		}
		log.info("Additional rent price: " + additionalRentPrice);
		
		log.info("returnRentedFilms finish");
				 				
		return additionalRentPrice;
				
	}
	
	/**
	 * Help method for calculating price
	 * 
	 * @param  filmCategory category of film
	 * @param  rentDays number of rent days
	 * @return price of film rent 
	 */
	private Integer calculatePrice(FilmCategory filmCategory, Integer rentDays) {
		
		Integer singlePrice = 0;
		
		if(filmCategory == FilmCategory.NEW) {
			singlePrice = config.getPremiumPrice() * rentDays;			
		}
		else if (filmCategory == FilmCategory.REGULAR) {
			singlePrice = config.getBasicPrice();
			if(rentDays > 3) {
				singlePrice += config.getBasicPrice() * (rentDays-3);
			}
		}
		else if (filmCategory == FilmCategory.OLD) {
			singlePrice += config.getBasicPrice();
			if(rentDays > 5) {
				singlePrice += config.getBasicPrice() * (rentDays-5);
			}
		}	
		
		return singlePrice;
		
	}
	
	/**
	 * Help method for calculating bonus points
	 * 
	 * @param  filmCategory category of film
	 * @param  bonusPoints existing bonus points
	 * @return bonusPoints existing bonus points increased 
	 */
	private Integer calculateBonusPoints(FilmCategory filmCategory, Integer bonusPoints)  {
				
		if(filmCategory == FilmCategory.NEW) {
			bonusPoints += config.getPointsNew();
		}
		else if (filmCategory == FilmCategory.REGULAR || filmCategory == FilmCategory.OLD) {
			bonusPoints += config.getPointsOther();
		}
		
		return bonusPoints;
	}

	
}
