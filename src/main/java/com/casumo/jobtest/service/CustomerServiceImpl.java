package com.casumo.jobtest.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casumo.jobtest.dto.CustomerDto;
import com.casumo.jobtest.dto.TopCustomerDto;
import com.casumo.jobtest.entity.Customer;
import com.casumo.jobtest.repository.CustomerRepository;

/**
 * Class for all actions related to Video store Customer 
 * 
 * @author Dean Meden
 */

@Service
public class CustomerServiceImpl implements CustomerService {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	CustomerRepository customerRepository;
	
	/**
	 * Creates a new customer in database
	 * 
	 * @param  customerDto DTO with data needed to create new customer
	 * @return Id of new customer
	 */	
	@Override
	public Long saveNewCustomer(CustomerDto customerDto) {
		
		log.info("saveNewCustomer: " + customerDto);
		
		Customer customer = new Customer();
		customer.setFirstName(customerDto.getFirstName());
		customer.setLastName(customerDto.getLastName());
		customer.setAddress(customerDto.getAddress());
		customer.setCreationTime(ZonedDateTime.now(ZoneId.systemDefault()));
		customer.setPoints(0);
		
		customer = customerRepository.saveAndFlush(customer);
		
		return customer.getId();
	}
	
	/**
	 * Get top 3 customer with most bonus points
	 * 
	 * @return List of top 3 customers
	 */
	
	@Override
	public List<TopCustomerDto> getTopCustomers() {
		
		log.info("getTopCustomers");
		
		List<Customer> customers = customerRepository.findTop3ByOrderByPointsDesc();
		List<TopCustomerDto> customersDtoList = new ArrayList<TopCustomerDto>();
		
		for (Customer customer : customers) {
			TopCustomerDto customersDto = new TopCustomerDto();
			customersDto.setCustomerId(customer.getId());
			customersDto.setBonusPoints(customer.getPoints());
			
			customersDtoList.add(customersDto);
		}
		
		log.info("Top customers: " + customersDtoList);
		
		return customersDtoList;
	}

}
