package com.casumo.jobtest.service;

import java.util.List;

import com.casumo.jobtest.dto.CustomerDto;
import com.casumo.jobtest.dto.TopCustomerDto;

/**
 * Interface for Customer service
 * 
 * @author Dean Meden
 */

public interface CustomerService {
	
	Long saveNewCustomer(CustomerDto customerDto); 
	
	List<TopCustomerDto> getTopCustomers();

}
