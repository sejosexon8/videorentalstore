package com.casumo.jobtest.service;

import java.util.List;

import com.casumo.jobtest.dto.RentFilmsDto;
import com.casumo.jobtest.dto.ReturnFilmsDto;

/**
 * Interface for Rental service
 * 
 * @author Dean Meden
 */

public interface RentalService {
	
	Integer saveRentedFilms(RentFilmsDto rentFilmsDto);
	
	Integer returnRentedFilms(List<ReturnFilmsDto> returnFilmsDtoList);
}
