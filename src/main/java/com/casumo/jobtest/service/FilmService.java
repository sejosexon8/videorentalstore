package com.casumo.jobtest.service;

import com.casumo.jobtest.dto.FilmDto;

/**
 * Interface for Film service
 * 
 * @author Dean Meden
 */

public interface FilmService {
	
	Long saveNewFilm(FilmDto filmDto); 

}
