package com.casumo.jobtest.dto;

/**
 * Class defines DTO data necessary to create new customer
 * 
 * @author Dean Meden
 */

public class CustomerDto {
	
	private String firstName;
	
	private String lastName;
	
	private String address;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "CustomerDto [" + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (address != null ? "address=" + address : "") + "]";
	}
		
}
