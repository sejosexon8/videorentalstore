package com.casumo.jobtest.dto;

/**
 * Class defines DTO data necessary to create new film
 * 
 * @author Dean Meden
 */
import com.casumo.jobtest.type.FilmCategory;

public class FilmDto {
	
	private String name;
	
	private FilmCategory category;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FilmCategory getCategory() {
		return category;
	}

	public void setCategory(FilmCategory category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "FilmDto [" + (name != null ? "name=" + name + ", " : "")
				+ (category != null ? "category=" + category : "") + "]";
	}
		
}
