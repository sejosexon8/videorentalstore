package com.casumo.jobtest.dto;

/**
 * Class defines DTO data necessary to get top customers
 * 
 * @author Dean Meden
 */
public class TopCustomerDto {
	
	private Long customerId;
	
	private Integer bonusPoints;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(Integer bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	@Override
	public String toString() {
		return "TopCustomerDto [" + (customerId != null ? "customerId=" + customerId + ", " : "")
				+ (bonusPoints != null ? "bonusPoints=" + bonusPoints : "") + "]";
	}
	
	

}
