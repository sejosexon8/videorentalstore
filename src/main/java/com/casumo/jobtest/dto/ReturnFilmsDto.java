package com.casumo.jobtest.dto;

/**
 * Class defines DTO data necessary to return film to rental store
 * 
 * @author Dean Meden
 */
public class ReturnFilmsDto {
	
	private Long rentalId;
	
	public Long getRentalId() {
		return rentalId;
	}

	public void setRentalId(Long rentalId) {
		this.rentalId = rentalId;
	}	

	@Override
	public String toString() {
		return "ReturnFilmsDto [" + (rentalId != null ? "rentalId=" + rentalId : "") + "]";
	}
	
}
