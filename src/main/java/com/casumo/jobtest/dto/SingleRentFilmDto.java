package com.casumo.jobtest.dto;

/**
 * Class defines DTO data necessary to rent film
 * 
 * @author Dean Meden
 */
public class SingleRentFilmDto {
	
	private Long filmId;
	
	private Integer rentalPeriodDays;

	public Long getFilmId() {
		return filmId;
	}

	public void setFilmId(Long filmId) {
		this.filmId = filmId;
	}

	public Integer getRentalPeriodDays() {
		return rentalPeriodDays;
	}

	public void setRentalPeriodDays(Integer rentalPeriodDays) {
		this.rentalPeriodDays = rentalPeriodDays;
	}

	@Override
	public String toString() {
		return "SingleRentFilmDto [" + (filmId != null ? "filmId=" + filmId + ", " : "")
				+ (rentalPeriodDays != null ? "rentalPeriodDays=" + rentalPeriodDays : "") + "]";
	}
	
}
