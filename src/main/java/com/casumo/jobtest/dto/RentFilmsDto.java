package com.casumo.jobtest.dto;

import java.util.List;

/**
 * Class defines DTO data necessary to rent multiple films
 * 
 * @author Dean Meden
 */
public class RentFilmsDto {
	
	private List<SingleRentFilmDto> films;	
	
	private Long customerId;
		
	public List<SingleRentFilmDto> getFilms() {
		return films;
	}

	public void setFilms(List<SingleRentFilmDto> films) {
		this.films = films;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "RentFilmsDto [" + (films != null ? "films=" + films + ", " : "")
				+ (customerId != null ? "customerId=" + customerId : "") + "]";
	}
	
}
