package com.casumo.jobtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.casumo.jobtest.entity.Rental;

/**
 * Class defines JPA Rental repository 
 * 
 * @author Dean Meden
 */

@Repository
public interface RentalRepository extends JpaRepository<Rental, Long> {
	
	Rental findById(Long id);
	
}
