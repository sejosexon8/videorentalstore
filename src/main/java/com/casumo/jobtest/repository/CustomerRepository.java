package com.casumo.jobtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.casumo.jobtest.entity.Customer;

/**
 * Class defines JPA Customer repository 
 * 
 * @author Dean Meden
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Customer findById(Long id);
	
	List<Customer> findTop3ByOrderByPointsDesc();
	
}
