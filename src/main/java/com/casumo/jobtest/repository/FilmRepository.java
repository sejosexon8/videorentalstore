package com.casumo.jobtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.casumo.jobtest.entity.Film;

/**
 * Class defines JPA Film repository 
 * 
 * @author Dean Meden
 */

@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {
	
	Film findById(Long id);
	
}
