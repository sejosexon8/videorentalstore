# Video rental store application #

This is video rental store backend application with exposed REST API. 

### What is this repository for? ###

In this repository Java backend code is stored with REST API, business logic, entities and tests.

### How do I get set up? ###

To setup project you need to pull the code from this repository and then run: 'mvnw spring-boot:run' inside project root directory.
Application REST API should be on http://localhost:8082/videoRentalStore/swagger-ui.html.
Also, Postgres database is used and videorentaldb database should be created on local machine (url: jdbc:postgresql://127.0.0.1:5432/videorentaldb).

### About application

Application serves as backend part of system and provides various methods for video rental store. 
Visual interface is provided with Swagger tool, it is simple for usage. 
Test are covering many cases, and they are separated in multiple classes. H2 in memory database is used to run tests.
Parameters such as bonus points for film type and price for film type are placed in application.properties file. 
Logging, JavaDoc are also added. 

### Possible improvements
There is still many services which can be added to make application more smarter such as deleting of customers, films, getting top films by number of rent.